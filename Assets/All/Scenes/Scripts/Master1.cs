using System.Threading;
using Unity.VisualScripting;
using UnityEngine;

public class Master1 : MonoBehaviour
{

    private void Update() {
        // Obtener todos los objetos con el tag "DragParent" en la escena
        GameObject[] dragParentObjects = GameObject.FindGameObjectsWithTag("DragParent");
        // Contar cu�ntos objetos se encontraron
        int count = dragParentObjects.Length;
       // Debug.Log("N�mero de objetos con el tag 'DragParent': " + count);
        if(count == 1)
        {
           LevelMaster2.lvl1 = true;
           MasterSound.PlayVictorySound();
           LevelMaster2.LoadRandomScene();
        }
    }
}
