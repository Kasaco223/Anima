using UnityEngine;

public class TutorialController : MonoBehaviour
{
    private Pause pauseScript;

    private void Start()
    {
        // Obtener una referencia al script Pause
        pauseScript = FindObjectOfType<Pause>();
        /*
        // Comprobar si se encontr� el script Pause
        if (pauseScript == null)
        {
            Debug.LogError("No se encontr� el script Pause en la escena.");
            return;
        }
        */
        // Pausar el juego al inicio
        pauseScript.PauseButton();
    }

    private void Update()
    {
        // Despausar el juego cuando se hace clic en la pantalla
        if (Input.GetMouseButtonDown(0)) // Si se presiona click en la pantalla
        {
            if (pauseScript != null)
            {
                // Despausar el juego y destruir este objeto
                pauseScript.PauseButton();
                Destroy(gameObject);
            }
            else
            {
                Debug.LogError("El script Pause no est� asignado en TutorialController.");
            }
        }
    }
}
